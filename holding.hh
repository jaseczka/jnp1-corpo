#include <iostream>

const unsigned int ACC_DEF_VAL = 15;
const unsigned int HS_DEF_VAL = 150;
const unsigned int EXO_DEF_VAL = 50;

template <unsigned int ACC, unsigned int HS, unsigned int EXO>
class Company {
	public:
		static const unsigned int ACC_NO = ACC;
		static const unsigned int HS_NO = HS;
		static const unsigned int EXO_NO = EXO;

};

typedef Company<1, 0, 0> Accountancy;
typedef Company<0, 1, 0> Hunting_shop;
typedef Company<0, 0, 1> Exchange_office;

/** Typ reprezentujący firmę powstałą z połączenia firm C1 i C2.
 */
template<class C1, class C2> struct add_comp
{
    typedef Company<C1::ACC_NO + C2::ACC_NO,
                    C1::HS_NO + C2::HS_NO,
                    C1::EXO_NO + C2::EXO_NO> type;
};

constexpr unsigned int init_sub(unsigned int lhs, unsigned int rhs)
{
    return lhs > rhs ? lhs - rhs : 0;
}

/** Typ reprezentujący firmę C1 pomniejszoną o firmę C2.
 */
template<class C1, class C2> struct remove_comp
{
    static const unsigned int ACC = init_sub(C1::ACC_NO, C2::ACC_NO);
    static const unsigned int HS = init_sub(C1::HS_NO, C2::HS_NO);
    static const unsigned int EXO = init_sub(C1::EXO_NO, C2::EXO_NO);

    typedef Company<ACC, HS, EXO> type;
};


/** Typ reprezentujący firmę powstałą z firmy C
 *  przez powiększenie n razy liczby poszczególnych przedsiębiorstw.
 */
template<class C, unsigned int n> struct multiply_comp
{
    typedef Company<C::ACC_NO * n,
                    C::HS_NO * n,
                    C::EXO_NO * n> type;
};

constexpr unsigned int init_div(unsigned int value, unsigned int n)
{
    return n > 0 ? value / n : 0;
}

/** Typ reprezentujący firmę powstałą z firmy C
 *  przez pomniejszenie n razy liczby poszczególnych przedsiębiorstw.
 */
template<class C, unsigned int n> struct split_comp
{
    static const unsigned int ACC = init_div(C::ACC_NO, n);
    static const unsigned int HS = init_div(C::HS_NO, n);
    static const unsigned int EXO = init_div(C::EXO_NO, n);

    typedef Company<ACC, HS, EXO> type;
};

/** Typ reprezentujący firmę powstałą z firmy C
 *  przez zwiększenie o jeden liczby wszystkich
 *  przedsiębiorstw wchodzących w skład firmy.
 */
template<class C> struct additive_expand_comp
{
    typedef Company<C::ACC_NO + 1,
                    C::HS_NO + 1,
                    C::EXO_NO + 1> type;
};

/** Typ reprezentujący firmę powstałą z firmy C
 *  przez zmniejszenie o jeden liczby wszystkich
 *  przedsiębiorstw wchodzących w skład firmy.
 */
template<class C> struct additive_rollup_comp
{
    static const unsigned int ACC = init_sub(C::ACC_NO, 1);
    static const unsigned int HS = init_sub(C::HS_NO, 1);
    static const unsigned int EXO = init_sub(C::EXO_NO, 1);

    typedef Company<ACC, HS, EXO> type;
};

template <class Company>
class Group {
private:
	unsigned int size;
	unsigned int acc_val;
	unsigned int hs_val;
	unsigned int exo_val;
public:

	//klasa Group<Company> powinna zawierać publiczną definicję
	//typu reprezentującego strukturę pojedynczej firmy
	//o nazwie company_type oraz
	//zmienną statyczną company typu company_type.
	typedef Company company_type;
	static company_type company;

	/** Tworzy grupę składającą się tylko z jednejfirmy.
	 */
	Group(): size(1), acc_val(ACC_DEF_VAL), hs_val(HS_DEF_VAL), exo_val(EXO_DEF_VAL)
	{
	}

	/** Tworzy grupę składającą się z k firm.
	 */
	Group(unsigned int k): size(k), acc_val(ACC_DEF_VAL), hs_val(HS_DEF_VAL), exo_val(EXO_DEF_VAL)
	{
	}

	/** Tworzy grupę identyczną z zadaną.
	 */
	Group(Group<Company> const &g): size(g.get_size()), acc_val(g.get_acc_val()), hs_val(g.get_hs_val()), exo_val(g.get_exo_val())
	{
	}

	/** Zwraca liczbę firm wchodzących w skład grupy
	 */
	unsigned int get_size() const
	{
	    return size;
	}

	/** Ustala wartość biura rachunkowego.
	 */
	void set_acc_val(unsigned int i)
	{
		this->acc_val = i;
	}

	/** Ustala wartość sklepu myśliwskiego.
	 */
	void set_hs_val(unsigned int i)
	{
		this->hs_val = i;
	}

	/** Ustala wartość kantoru.
	 */
	void set_exo_val(unsigned int i)
	{
		this->exo_val = i;
	}

	unsigned int get_acc_val() const
	{
		return this->acc_val;
	}
	unsigned int get_hs_val() const
	{
		return this->hs_val;
	}
	unsigned int get_exo_val() const
	{
		return this->exo_val;
	}

	/** Suma wartości przedsiębiorstw wchodzących w skład grupy
	 */
	unsigned int get_value() const
	{
		unsigned int hs = hs_val * Company::HS_NO * size;
		unsigned int exo = exo_val * Company::EXO_NO * size;
		unsigned int acc = acc_val * Company::ACC_NO * size;
		return hs + exo + acc;
	}


	Group<Company> operator+(const Group<Company> &g2)
	{
		Group<Company> g(this->get_size() + g2.get_size());
		if (g.get_size() == 0){
			g.set_hs_val(0);
			g.set_acc_val(0);
			g.set_exo_val(0);
		}
		else {
			g.set_hs_val(
				(this->get_hs_val() * this->get_size() + g2.get_hs_val() * g2.get_size())
				/ g.get_size());
			g.set_acc_val(
				(this->get_acc_val() * this->get_size() + g2.get_acc_val() * g2.get_size())
				/ g.get_size());
			g.set_exo_val(
				(this->get_exo_val() * this->get_size() + g2.get_exo_val() * g2.get_size())
				/ g.get_size());
		}
		return g;
	}
	Group<Company> operator+=(const Group<Company> &g2)
	{
		unsigned int s = this->get_size() + g2.get_size();
		if (s == 0){
			this->set_hs_val(0);
			this->set_acc_val(0);
			this->set_exo_val(0);
		}
		else {
			this->set_hs_val(
				(this->get_hs_val() * this->get_size() + g2.get_hs_val() * g2.get_size())
				/ s);
			this->set_acc_val(
				(this->get_acc_val() * this->get_size() + g2.get_acc_val() * g2.get_size())
				/ s);
			this->set_exo_val(
				(this->get_exo_val() * this->get_size() + g2.get_exo_val() * g2.get_size())
				/ s);
		}
		this->size = s;
		return *this;
	}

	Group<Company> operator-(const Group<Company> &g2)
	{
		int s = this->get_size() - g2.get_size();
		if (s < 0)
			s = 0;
		Group<Company> g(s);
		if (g.get_size() == 0){
			g.set_hs_val(0);
			g.set_acc_val(0);
			g.set_exo_val(0);
		}
		else {
			g.set_hs_val(
				std::max((this->get_hs_val() * this->get_size() - g2.get_hs_val() * g2.get_size())
				/ g.get_size(), 0u)
				);
			g.set_acc_val(
				std::max((this->get_acc_val() * this->get_size() - g2.get_acc_val() * g2.get_size())
				/ g.get_size(), 0u)
				);
			g.set_exo_val(
				std::max((this->get_exo_val() * this->get_size() - g2.get_exo_val() * g2.get_size())
				/ g.get_size(), 0u)
				);
		}
		return g;
	}


	Group<Company> operator-=(const Group<Company> &g2)
	{
		int s = this->get_size() - g2.get_size();
		if (s < 0)
			s = 0;
		if (s == 0){
			this->set_hs_val(0);
			this->set_acc_val(0);
			this->set_exo_val(0);
		}
		else {
			this->set_hs_val(
				max((this->get_hs_val() * this->get_size() - g2.get_hs_val() * g2.get_size())
				/ s, 0u)
				);
			this->set_acc_val(
				max((this->get_acc_val() * this->get_size() - g2.get_acc_val() * g2.get_size())
				/ s, 0u)
				);
			this->set_exo_val(
				max((this->get_exo_val() * this->get_size() - g2.get_exo_val() * g2.get_size())
				/ s, 0u)
				);
		}
		this->size = s;
		return *this;
	}

	void operator *=(unsigned int i)
	{
		if (i == 0){
			this->set_hs_val(0);
			this->set_exo_val(0);
			this->set_acc_val(0);
		}
		else {
			this->set_hs_val(this->get_hs_val() / i);
			this->set_exo_val(this->get_exo_val() / i);
			this->set_acc_val(this->get_acc_val() / i);
		}
		this->size *= i;
	}

	void operator /=(unsigned int i)
	{
		if (i == 0){
			this->size = 0;
		}
		else {
			this->size /= i;
		}
		this->set_hs_val(this->get_hs_val() * i);
		this->set_exo_val(this->get_exo_val() * i);
		this->set_acc_val(this->get_acc_val() * i);

	}

	Group<Company> operator*(unsigned int i)
	{
		Group<Company> g(*this);
		g *= i;
		return g;
	}

	Group<Company> operator/(unsigned int i)
	{
		Group<Company> g(*this);
		g /= i;
		return g;
	}


	//Operatory porównywania grup firm: ==, !=, <, >, <=, >=.
	//Pamiętaj, że w krytycznych sytuacjach nie jest istotne,
	//ile mamy biur rachunkowych. Liczy się tylko
	//liczba kantorów (czyli kasa) oraz
	//sklepów myśliwskich (broń). Posiadanie
	//większej liczby przedsiębiorstw tych dwóch typów czyni nas
	//silniejszymi od przeciwnika. Porządek na grupach nie jest liniowy.

	//Operator << wypisujący na strumień opis grupy.
	template<class C> friend std::ostream & operator<<(std::ostream & out, Group<C> const &g);
};

template<class C> std::ostream & operator<<(std::ostream & out, Group<C> const &g)
{
	out << "Number of companies: " << g.get_size() << "; Value: " << g.get_value() << std::endl;
	out << "Accountancies value: " << g.get_acc_val();
	out << ", Hunting shops value: " << g.get_hs_val();
	out << ", Exchange offices value: " << g.get_exo_val() << std::endl;
	out << "Accountancies: " << C::ACC_NO;
	out << ", Hunting shops: " << C::HS_NO;
	out << ", Exchange offices: " << C::EXO_NO << std::endl;
}

template<class C1, class C2>bool operator==(Group<C1> const &g1, Group<C2> const &g2)
{
	if (
		(g1.get_size() * C1::EXO_NO == g2.get_size() * C2::EXO_NO)
		&&
		(g1.get_size() * C1::HS_NO == g2.get_size() * C2::HS_NO)
		)
		return true;

	return false;
}

template<class C1, class C2>bool operator!=(Group<C1> const &g1, Group<C2> const &g2)
{
	return !(g1==g2);
}

template<class C1, class C2>bool operator<(Group<C1> const &g1, Group<C2> const &g2)
{
	if (
		(g1.get_size() * C1::EXO_NO < g2.get_size() * C2::EXO_NO)
		&&
		(g1.get_size() * C1::HS_NO < g2.get_size() * C2::HS_NO)
		)
		return true;

	return false;
}


template<class C1, class C2>bool operator>(Group<C1> const &g1, Group<C2> const &g2)
{
	if (
		(g1.get_size() * C1::EXO_NO > g2.get_size() * C2::EXO_NO)
		&&
		(g1.get_size() * C1::HS_NO > g2.get_size() * C2::HS_NO)
		)
		return true;

	return false;
}

template<class C1, class C2>bool operator<=(Group<C1> const &g1, Group<C2> const &g2)
{
	return (g1 < g2) || (g1==g2);
}


template<class C1, class C2>bool operator>=(Group<C1> const &g1, Group<C2> const &g2)
{
	return (g1 > g2) || (g1==g2);
}


/** Zwiększa o jeden liczbę przedsiębiorstw
 *  (wszystkich typów) wchodzących w skład
 *  każdej firmy należącej do grupy s1,
 *  nie zmieniając wartości pojedynczego przedsiębiorstwa.
 */
template<class C>
Group<typename additive_expand_comp<C>::type> const
additive_expand_group(Group<C> const &s1)
{
    Group<typename additive_expand_comp<C>::type> s2(s1.get_size());
    s2.set_acc_val(s1.get_acc_val());
    s2.set_hs_val(s1.get_hs_val());
    s2.set_exo_val(s1.get_exo_val());

    return s2;
}

/** Zwiększa dziesięciokrotnie liczbę przedsiębiorstw 
 *  (wszystkich typów) wchodzących w skład każdej 
 *  firmy należącej do grupy s1, nie zmieniając 
 *  wartości pojedynczego przedsiębiorstwa.
 */
template<class C>
Group<typename multiply_comp<C, 10>::type> const
multiplicative_expand_group(Group<C> const &s1)
{
    Group<typename multiply_comp<C, 10>::type> s2(s1.get_size());
    s2.set_acc_val(s1.get_acc_val());
    s2.set_hs_val(s1.get_hs_val());
    s2.set_exo_val(s1.get_exo_val());

    return s2;
}

/** Zmniejsza o jeden liczbę przedsiębiorstw 
 *  (wszystkich typów) wchodzących w skład 
 *  każdej firmy należącej do grupy s1, 
 *  nie zmieniając wartości pojedynczego przedsiębiorstwa.
 */
template<class C>
Group<typename additive_rollup_comp<C>::type> const
additive_rollup_group(Group<C> const &s1)
{
    Group<typename additive_rollup_comp<C>::type> s2(s1.get_size());
    s2.set_acc_val(s1.get_acc_val());
    s2.set_hs_val(s1.get_hs_val());
    s2.set_exo_val(s1.get_exo_val());

    return s2;
}

/** Zmniejsza dziesięciokrotnie liczbę przedsiębiorstw 
 *  (wszystkich typów) wchodzących w skład każdej firmy 
 *  należącej do grupy s1, nie zmieniając wartości 
 *  pojedynczego przedsiębiorstwa.
 */
template<class C>
Group<typename split_comp<C, 10>::type> const
multiplicative_rollup_group(Group<C> const &s1)
{
    Group<typename split_comp<C, 10>::type> s2(s1.get_size());
    s2.set_acc_val(s1.get_acc_val());
    s2.set_hs_val(s1.get_hs_val());
    s2.set_exo_val(s1.get_exo_val());

    return s2;
}

/** Funkcja, która pomoże nam określać, czy możliwe jest 
 * wyłonienie zwycięzcy przetargu (nie zawsze 
 * jest to możliwe) w przypadku, gdy startują w nim grupy g1, g2 
 * oraz g3. Zwycięzcą zostaje grupa, która jest największa 
 * w sensie porządku zdefiniowanego na grupach.
 */
template<class C1, class C2, class C3>
bool
solve_auction(Group<C1> const &g1, Group<C2> const &g2, Group<C3> const &g3)
{
	if ((g1 > g2) && (g1 > g3))
		return true;
	if ((g3 > g2) && (g3 > g1))
		return true;
	if ((g2 > g1) && (g2 > g3))
		return true;
	return false;
}
